<?

class my_class
{
	public $handle;
	public $title;
	public $option1_name;
	public $option1_value;
	public $option2_name;
	public $option2_value;
	public $option3_name;
	public $option3_value;
	public $sku;
	public $hs_code;
	public $coo;
	public $twarda;
	public $russia;
	public $usa;
	public $poland;
	public $uae;
	public $cad;

	public function __construct($handle, $title, $option1_name, $option1_value, $option2_name, $option2_value, $option3_name, $option3_value, $sku, $hs_code, $coo, $twarda, $russia, $usa, $poland, $uae, $cad)
		{
			$this->handle=$handle;
			$this->title=$title;
			$this->option1_name=$option1_name;
			$this->option1_value=$option1_value;
			$this->option2_name=$option2_name;
			$this->option2_value=$option2_value;
			$this->option3_name=$option3_name;
			$this->option3_value=$option3_value;
			$this->sku=$sku;
			$this->hs_code=$hs_code;
			$this->coo=$coo;
			$this->twarda=$twarda;
			$this->russia=$russia;
			$this->usa=$usa;
			$this->poland=$poland;
			$this->uae=$uae;
			$this->cad=$cad;
		}
}

function get_file($file_name)
	{
		$file=fopen($file_name, 'r');
		$array=array();
		while(($line=fgetcsv($file))!==FALSE)
			{
				$array[]=new my_class($line[0], $line[1], $line[2], $line[3], $line[4], $line[5], $line[6], $line[7], $line[8], $line[9], $line[10], $line[11], $line[12], $line[13], $line[14], $line[15], $line[16]);
			}
		fclose($file);
		return $array;
	}

function write_file($file_name, $array)
	{
		$file=fopen($file_name, 'w');
		foreach($array as $key=>$value)
			{
				$string=$value->handle.','.$value->title.','.$value->option1_name.','.$value->option1_value.','.$value->option2_name.','.$value->option2_value.','.$value->option3_name.','.$value->option3_value.','.$value->sku.','.$value->hs_code.','.$value->coo.','.$value->twarda.','.$value->russia.','.$value->usa.','.$value->poland.','.$value->uae.','.$value->cad;
				fwrite($file, $string.PHP_EOL);
			}
		fclose($file);
	}

$array_1=get_file('Itsmiladress_inventory.csv');
$array_2=get_file('itsmilla_all_inventory.csv');

$onovleno=0;
foreach($array_1 as $key_1=>$value_1)
	{
		$obrobleno=0;
		if($key_1>0)
			{
				foreach($array_2 as $key_2=>$value_2)
					{
						if($key_2>0)
							{
								if(strcmp(str_replace('-black-friday','',$value_1->handle), str_replace('-black-friday','',$value_2->handle))==0 and strcmp($value_1->option1_name, $value_2->option1_name)==0 and strcmp($value_1->option1_value, $value_2->option1_value)==0 and strcmp($value_1->option2_name, $value_2->option2_name)==0 and strcmp($value_1->option2_value, $value_2->option2_value)==0 and strcmp($value_1->option3_name, $value_2->option3_name)==0 and strcmp($value_1->option3_value, $value_2->option3_value)==0)
									{
										$array_2[$key_2]->twarda=$array_1[$key_1]->twarda;
										$onovleno++;
									}
								$obrobleno++;
							}
					}
			}
	}


write_file('file_result.csv', $array_2);
file_put_contents('logs.txt', 'оброблено: '.$obrobleno.PHP_EOL.'оновлено: '.$onovleno);
print_r('оброблено: '.$obrobleno.'<br>оновлено: '.$onovleno);

?>